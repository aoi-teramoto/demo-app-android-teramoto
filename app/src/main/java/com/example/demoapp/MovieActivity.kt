package com.example.demoapp

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_movie.*

class MovieActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        Handler(mainLooper).postDelayed({
            var moviePath = Uri.parse("android.resource://" + packageName + "/" + R.raw.glass)
            videoView.setVideoURI(moviePath)

            videoView.setOnPreparedListener {
                videoView.start()

                videoView.setMediaController(MediaController(this))
            }

            videoView.setOnCompletionListener {
                finish()
            }
        },200)
    }
}


