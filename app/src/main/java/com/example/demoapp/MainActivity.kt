package com.example.demoapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

        // LayoutManagerの設定
        val layoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = layoutManager

        // Adapterの設定
        val sampleList = mutableListOf<String>(
            "Input Text", "Slider", "Drag And Drop", "Map View",
            "Play Video", "Web View", "View Pager", "Grid View", "Unquete", "Scroll"
        )
        val adapter = RecyclerViewAdapter(sampleList)
        recycler_view.adapter = adapter

        // インターフェースの実装
        adapter.setOnItemClickListener(object:RecyclerViewAdapter.OnItemClickListener{
            override fun onItemClickListener(view: View, position: Int, clickedText: String) =
                if(clickedText == "Slider") {
                    val intent = Intent(applicationContext, PickerActivity::class.java)
                    startActivity(intent)
            } else if(clickedText == "Map View") {
                    val intent = Intent(applicationContext, MapActivity::class.java)
                    startActivity(intent)
                } else if(clickedText == "Play Video") {
                    val intent = Intent(applicationContext, MovieActivity::class.java)
                    startActivity(intent)
                } else {

                }
        })
    }