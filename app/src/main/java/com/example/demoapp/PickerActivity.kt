package com.example.demoapp

import android.graphics.Color
import android.os.Bundle
import android.widget.SeekBar
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_picker.*


class PickerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)

        val sb=listOf(sbR,sbG,sbB)

        sb.forEach(){
            it.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
                override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                    backgroundColorRedraw()
                }
                override fun onStartTrackingTouch(p0: SeekBar?) {}
                override fun onStopTrackingTouch(p0: SeekBar?) {}
            })

        }
        backgroundColorRedraw()
    }
    fun backgroundColorRedraw(){
        backgroundColor.setBackgroundColor(Color.rgb(sbR.progress,sbG.progress,sbB.progress))
    }
}